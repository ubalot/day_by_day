#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "activity.h"

void display_activity(const activity ac)
{
        struct tm *date;

        date = localtime(&ac.time);

        printf( "title: %s\n"
                "points: %ld\n"
                "category: %s\n"
                "done: %s\n"
                "date: %d %d %d \n"
                "time: %ld\n",
                ac.title,
                ac.points,
                ac.category,
                ac.done == true ? "true" : "false",
                1900 + date->tm_year, date->tm_mon, date->tm_mday,
                ac.time);
}

activity create_activity(char *title, size_t points, bool done, char *category)
{
        activity ac;

        strncpy(ac.title, title, MAX_TITLE_LENGTH);
        ac.points = points;
        ac.done = done;
        strncpy(ac.category, category, MAX_CATEGORY_LENGTH);
        ac.time = NULL_TIME;

        return ac;
}

activity *todo_activity(activity *ac)
{
        ac->time = NULL_TIME;
        ac->done = false;

        return ac;
}

activity *complete_activity(activity *ac)
{
        time(&ac->time);
        ac->done = true;

        return ac;
}
