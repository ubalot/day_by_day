#ifndef ACTIVITIES_H
#define ACTIVITIES_H

#include <stdbool.h>

#include "activity.h"
#include "categories.h"

#define ACTIVITIES_PATH "./assets/activities.txt"

typedef struct list {
        activity ac;
        struct list *next;
} activities_list;

activities_list *get_activities();
bool append_activity(activities_list **acl, const activity ac);
activity *remove_activity(activities_list **acl, char *title);
bool save_activities(activities_list *activities);
void display_activities(activities_list *activities);
bool reset_activities();
activity *activity_done(activities_list *activities, char *title);
void activities_check();

#endif