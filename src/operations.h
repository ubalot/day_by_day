#ifndef OPERATIONS_H
#define OPERATIONS_H

typedef int operation;

#define UNKNOWN_OPERATION  0
#define ADD_ACTIVITY       1
#define DELETE_ACTIVITY    2
#define LIST_ACTIVITIES    3
#define SHOW_SCORES        4
#define SET_SCORES         5
#define RESET_ACTIVITIES   6
#define COMPLETE_ACTIVITY  7
#define QUIT               99

#endif
