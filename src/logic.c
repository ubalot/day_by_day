#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logic.h"


operation interactive()
{
        char response[20];

        printf( "What action would you like to perform?\n"
                "Choose between:\n"
                "- Add an activity:         write 'a' or 'add'\n"
                "- Complete an activity:    write 'c' or 'complete'\n"
                "- Delete an activity:      write 'd' or 'delete'\n"
                "- List activities:         write 'l' or 'list'\n"
                "- Show scores:             write 's' or 'scores'\n"
                "- Set scores:              write 't' or 'target'\n"
                "- Reset activities:        write 'r' or 'reset'\n"
                "- Quit:                    write 'q' or 'quit'\n"
                "> ");

        if (scanf("%s", response) <= 0) {
                perror("Error reading user input");
                exit(EXIT_FAILURE);
        }

        if (strcmp(response, "a") == 0 || strcmp(response, "add") == 0)
                return ADD_ACTIVITY;

        if (strcmp(response, "c") == 0 || strcmp(response, "complete") == 0)
                return COMPLETE_ACTIVITY;

        if (strcmp(response, "d") == 0 || strcmp(response, "delete") == 0)
                return DELETE_ACTIVITY;

        if (strcmp(response, "l") == 0 || strcmp(response, "list") == 0)
                return LIST_ACTIVITIES;

        if (strcmp(response, "s") == 0 || strcmp(response, "scores") == 0)
                return SHOW_SCORES;

        if (strcmp(response, "t") == 0 || strcmp(response, "target") == 0)
                return SET_SCORES;

        if (strcmp(response, "r") == 0 || strcmp(response, "reset") == 0)
                return RESET_ACTIVITIES;

        if (strcmp(response, "q") == 0 || strcmp(response, "quit") == 0)
                return QUIT;

        return UNKNOWN_OPERATION;
}

activity *interactive_add()
{
        activity* ac = (activity*)malloc(sizeof(activity));
        char title[MAX_TITLE_LENGTH];
        size_t points = 0;
        char category[MAX_CATEGORY_LENGTH];

        printf( "Choose a title for your new activity.\n"
                "> ");

        if (scanf("%s", title) <= 0) {
                perror("Error reading user input");
                exit(EXIT_FAILURE);
        }

        printf( "Assign an integer to the score of '%s' activity.\n"
                "> ", title);


        if (scanf("%lud", &points) <= 0) {
                perror("Error reading user input");
                exit(EXIT_FAILURE);
        }

        printf( "Assign a category to '%s' activity.\n"
                "[%s[d]/%s[w]/%s[m]/%s[y]]> ", title, DAILY, WEEKLY, MONTHLY, YEARLY);

        if (scanf("%s", category) <= 0) {
                perror("Error reading user input");
                exit(EXIT_FAILURE);
        }

        /* Expand shortcuts for category. */
        if (strcmp(category, "d") == 0)
                strcpy(category, DAILY);
        else if (strcmp(category, "w") == 0)
                strcpy(category, WEEKLY);
        else if (strcmp(category, "m") == 0)
                strcpy(category, MONTHLY);
        else if (strcmp(category, "y") == 0)
                strcpy(category, YEARLY);

        *ac = create_activity(title, points, false, category);

        return ac;
}

char *interactive_complete_activity(char* title)
{
        if (title == NULL) {
                perror("NULL pointer.");
                exit(EXIT_FAILURE);
        }

        printf( "Write the title of the activity you have done.\n"
                "> ");

        if (scanf("%s", title) <= 0) {
                perror("Error while reading user input");
                exit(EXIT_FAILURE);
        }

        return title;
}

char *interactive_delete(char *title)
{
        if (title == NULL) {
                perror("String is a NULL pointer\n");
                return NULL;
        }

        printf( "Delete activity with title ~> ");

        if (scanf("%s", title) <= 0) {
                perror("Error reading user input\n");
                exit(EXIT_FAILURE);
        }

        return title;
}

configurations *interactive_set_scores(configurations *config)
{
        char target;
        size_t score;

        if (config == NULL) {
                perror("Config is a NULL pointer\n");
                return NULL;
        }

        printf("What score would you like to change?[d|w|m|y]: ");
        if (scanf("%s", &target) <= 0) {
                perror("Error reading user input: expects a string.\n");
                exit(EXIT_FAILURE);
        }

        printf("Insert the new target score: ");
        if (scanf("%lud", &score) <= 0) {
                perror("Error reading user input: expects a number.\n");
                exit(EXIT_FAILURE);
        }

        switch (target) {
        case 'd':
                config->target_day_score = score;
                break;

        case 'w':
                config->target_week_score = score;
                break;

        case 'm':
                config->target_month_score = score;
                break;

        case 'y':
                config->target_year_score = score;
                break;

        default:
                perror("Wrong value.\n");
                break;
        }

        return config;
}
