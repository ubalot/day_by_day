#ifndef LOGIC_H
#define LOGIC_H

#include "activity.h"
#include "activities.h"
#include "categories.h"
#include "config.h"
#include "operations.h"


operation interactive();
activity *interactive_add();
char *interactive_complete_activity(char *title);
char *interactive_delete(char *title);
configurations *interactive_set_scores(configurations *config);

#endif
