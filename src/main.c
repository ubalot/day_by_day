#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "activities.h"
#include "logic.h"
#include "config.h"

#define DEBUG

/*
 * Launch program in interactive mode: ask user for input.
 */
int interactive_mode()
{
        operation op;
        configurations *config;
        activity *ac;
        activities_list *activities;
        char title[MAX_TITLE_LENGTH];

        while (true) {
                switch ((op = interactive())) {
                case ADD_ACTIVITY:
                        if ((ac = interactive_add()) == NULL) {
                                printf("Error while creating activity\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if (append_activity(&activities, *ac) == false) {
                                printf("Error while adding %s to activites\n", ac->title);
                                return EXIT_FAILURE;
                        }

                        /* Update activities file. */
                        if (save_activities(activities) == true) {
                                printf("Added activity '%s'\n\n", ac->title);
                        } else {
                                printf("Error while adding activity '%s'\n\n", ac->title);
                                return EXIT_FAILURE;
                        }

                        free(ac);
                        free(activities);

                        break;

                case COMPLETE_ACTIVITY:
                        if (interactive_complete_activity(title) == NULL) {
                                printf("Error while reading user input\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if ((ac = activity_done(activities, title)) != NULL) {
                                printf("\n");
                                display_activity(*ac);
                                printf("\n");
                        } else {
                                printf("Activity '%s' wasn't modified.\n", title);
                                return EXIT_FAILURE;
                        }

                        if (!save_activities(activities)) {
                                printf("Error while editing activity '%s'\n\n", ac->title);
                                return EXIT_FAILURE;
                        }

                        free(activities);

                        break;

                case DELETE_ACTIVITY:
                        if (interactive_delete(title) == NULL) {
                                printf("Error while deleting activity\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if ((ac = remove_activity(&activities, title)) == NULL) {
                                perror("Error while removing activity\n");
                                return EXIT_FAILURE;
                        }

                        /* Update activities file. */
                        if (save_activities(activities) == true) {
                                printf("\n");
                                printf("Removed activity:\n");
                                display_activity(*ac);
                                printf("\n");
                        } else {
                                perror("\nError while updating activities.\n");
                                return EXIT_FAILURE;
                        }

                        free(ac);
                        free(activities);

                        break;

                case LIST_ACTIVITIES:
                        activities = get_activities();

                        printf("\n");
                        display_activities(activities);

                        free(activities);

                        break;

                case RESET_ACTIVITIES:
                        if (reset_activities() == true)
                                printf("\nActivties are empty now.\n\n");
                        else
                                printf("\nThere was a problem while deleting all activities.\n\n");

                        break;

                case SHOW_SCORES:
                        if ((config = read_settings()) == NULL) {
                                perror("Error while reading configurations\n");
                                return EXIT_FAILURE;
                        }

                        printf("\n");
                        display_config(*config);
                        printf("\n");

                        free(config);

                        break;

                case SET_SCORES:
                        if ((config = read_settings()) == NULL) {
                                perror("Error while reading configurations\n");
                                return EXIT_FAILURE;
                        }

                        printf("\n");
                        if (interactive_set_scores(config) == NULL) {
                                perror("Fail to get user input.\n");
                                return EXIT_FAILURE;
                        }
                        write_settings(config);
                        printf("\n");

                        free(config);

                        break;

                case QUIT:
                        printf("\nquitting...\n\n");
                        return EXIT_SUCCESS;

                default:
                        printf("\nUnknown operation.\n\n");
                        break;
                }
        }

        return EXIT_SUCCESS;
}

static void print_usage(const char *program_name)
{
        printf( "Usage: %s [-a|--add] [-c|--complete <title>] [-d|--delete <title>] [-l|--list [<category>]] [-s|--set <limit>] [--reset] [--help]\n"
                "\t\t-a|--add\tAdd an ativity.\n"
                "\t\t-c|--complete\tComplete an ativity.\n"
                "\t\t-d|--delete\tDelete an ativity.\n"
                "\t\t-l|--list\tList all ativities.\n"
                "\t\t-i|--interactive\tGet a prompt for interactive commands\n"
                "\t\t-g|--global\tSet global score\n"
                "\t\t--reset\t\tClear all notes.\n"
                "\t\t--help\t\tPrint help message.\n", program_name);
}

int command_line_mode(int argc, char **argv)
{
        char *program_name = argv[0];
        int opt;
        int longindex = 0;

        /*
         * put ':' in the starting of the
         * string so that program can
         * distinguish between '?' and ':'
         */
        char *short_options = ":ad:l::s";

        struct option long_options[] =
        {
                {"add",      no_argument,       0, 'a'},
                {"complete", required_argument, 0, 'c'},
                {"delete",   required_argument, 0, 'd'},
                {"list",     optional_argument, 0, 'l'},
                {"config",   no_argument,       0, 's'},
                {"target",   required_argument, 0, 't'},
                {"reset",    no_argument,       0, 'r'},
                {"help",     no_argument,       0, 'h'}
        };

        bool add_option_flag = false;

        configurations *config;
        activity *ac;
        activities_list *activities;
        char title[MAX_TITLE_LENGTH];

        while((opt = getopt_long(argc, argv, short_options, long_options, &longindex)) != -1)
        {
                activities_check();

                switch(opt)
                {
                case 'h':
                        print_usage(program_name);

                        break;

                case 'a':
                        if ((ac = interactive_add()) == NULL) {
                                printf("Error while creating activity\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if (append_activity(&activities, *ac) == false) {
                                printf("Error while adding %s to activites\n", ac->title);
                                return EXIT_FAILURE;
                        }

                        /* Update activities file. */
                        if (save_activities(activities) == true) {
                                printf("Added activity '%s'\n\n", ac->title);
                        } else {
                                printf("Error while adding activity '%s'\n\n", ac->title);
                                return EXIT_FAILURE;
                        }

                        free(ac);
                        free(activities);

                        break;

                case 'c':
                        if (interactive_complete_activity(title) == NULL) {
                                printf("Error while reading user input\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if ((ac = activity_done(activities, title)) != NULL) {
                                display_activity(*ac);
                        } else {
                                printf("Activity '%s' wasn't modified.\n", title);
                                return EXIT_FAILURE;
                        }

                        free(activities);

                        break;

                case 'd':
                        if (interactive_delete(title) == NULL) {
                                printf("Error while deleting activity\n");
                                return EXIT_FAILURE;
                        }

                        activities = get_activities();

                        if ((ac = remove_activity(&activities, title)) == NULL) {
                                perror("Error while removing activity\n");
                                return EXIT_FAILURE;
                        }

                        /* Update activities file. */
                        if (save_activities(activities) == true) {
                                printf("Removed activity:\n");
                                display_activity(*ac);
                        } else {
                                perror("Error while updating activities.\n");
                                return EXIT_FAILURE;
                        }

                        free(ac);
                        free(activities);

                        break;

                case 'l':
                        activities = get_activities();

                        printf("\n");
                        display_activities(activities);

                        free(activities);

                        break;

                case 's':
                        if ((config = read_settings()) == NULL) {
                                perror("Error while reading configurations\n");
                                return EXIT_FAILURE;
                        }

                        printf("\n");
                        display_config(*config);
                        printf("\n");

                        free(activities);

                        break;

                case 't':
                        if ((config = read_settings()) == NULL) {
                                perror("Error while reading configurations\n");
                                return EXIT_FAILURE;
                        }

                        printf("\n");
                        if (interactive_set_scores(config) == NULL) {
                                perror("Fail to get user input.\n");
                                return EXIT_FAILURE;
                        }
                        write_settings(config);
                        printf("\n");

                        break;

                case 'r':
                        if (reset_activities() == true)
                                printf("\nActivties are empty now.\n\n");
                        else
                                printf("\nThere was a problem while deleting all activities.\n\n");

                        break;

                case ':':
                        printf("option %c needs a value\n", opt);
                        return EXIT_FAILURE;

                case '?':
                default:
                        print_usage(program_name);
                        return EXIT_FAILURE;
                }
        }

        /*
         * optind is for the extra arguments
         * which are not parsed
         */
        if (add_option_flag == true) {
                for (; optind < argc; optind++) {
                        printf("extra arguments: %s\n", argv[optind]);
                }
        }

        return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
        /* Set default configurations if file doesn't exist. */
        if (access(CONFIG_PATH, F_OK) == -1)
                create_default_configs();

        /* Mark as undone activities related to past epocs. */
        activities_check();

        /* Actual program launcher. */
        return argc == 1 ? interactive_mode() : command_line_mode(argc, argv);
}
