#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <libgen.h>
#include <linux/limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"

#define BUFFER_SIZE 1024

void display_config(configurations config)
{
        printf( "Target day score: %ld\n"
                "Target week score: %ld \n"
                "Target month score: %ld\n"
                "Target year score: %ld\n"
                "\n"
                "Current day score: %ld\n"
                "Current week score: %ld\n"
                "Current month score: %ld\n"
                "Current year score: %ld\n",
                config.target_day_score, config.target_week_score, config.target_month_score,  config.target_year_score,
                config.current_day_score, config.current_week_score, config.current_month_score, config.current_year_score);
}

/*
 * Read one line from configuration file and assign value to the right
 * field in struct 'configurations'
 */
static void assign_value(configurations *config, char* buffer)
{
        char *delim = "=";
        char *token;
        int n;

        token = strtok(buffer, delim);
        n = strlen(token) + 1;  /* ????????? */
        if (strncmp(token, TARGET_DAY_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->target_day_score = atoi(token);
        } else if (strncmp(token, TARGET_WEEK_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->target_week_score = atoi(token);
        } else if (strncmp(token, TARGET_MONTH_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->target_month_score = atoi(token);
        } else if (strncmp(token, TARGET_YEAR_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->target_year_score = atoi(token);
        } else if (strncmp(token, CURRENT_DAY_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->current_day_score = atoi(token);
        } else if (strncmp(token, CURRENT_WEEK_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->current_week_score = atoi(token);
        } else if (strncmp(token, CURRENT_MONTH_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->current_month_score = atoi(token);
        } else if (strncmp(token, CURRENT_YEAR_SCORE, n) == 0) {
                token = strtok(NULL, delim);  /* read value after '=' */
                config->current_year_score = atoi(token);
        }
}

/*
 * Read configuration file line by line.
 * Return a struct 'configuration' with initialized fields.
 */
configurations *read_settings()
{
        FILE *file;
        char buffer[BUFFER_SIZE];
        configurations *config = (configurations*)malloc(sizeof(configurations));

        if ((file = fopen(CONFIG_PATH, "r")) == NULL) {
                printf("%s\n", strerror(errno));
                return NULL;
        }

        while (fgets(buffer, BUFFER_SIZE, file) != NULL)
                assign_value(config, buffer);

        fclose(file);

        return config;
}

/*
 * Write configurations into file.
 */
bool write_settings(const configurations *config)
{
        FILE *file;

        if ((file = fopen(CONFIG_PATH, "w")) == NULL) {
                printf("%s\n", strerror(errno));
                return false;
        }

        fprintf(file, "%s=%ld\n", TARGET_DAY_SCORE,   config->target_day_score);
        fprintf(file, "%s=%ld\n", TARGET_WEEK_SCORE,  config->target_week_score);
        fprintf(file, "%s=%ld\n", TARGET_MONTH_SCORE, config->target_month_score);
        fprintf(file, "%s=%ld\n", TARGET_YEAR_SCORE,  config->target_year_score);
        fprintf(file, "\n");
        fprintf(file, "%s=%ld\n", CURRENT_DAY_SCORE,   config->current_day_score);
        fprintf(file, "%s=%ld\n", CURRENT_MONTH_SCORE, config->current_week_score);
        fprintf(file, "%s=%ld\n", CURRENT_MONTH_SCORE, config->current_month_score);
        fprintf(file, "%s=%ld\n", CURRENT_YEAR_SCORE,  config->current_year_score);

        fclose(file);

        return true;
}

void create_default_configs()
{
        configurations config;

        config.target_day_score   = 0;
        config.target_week_score  = 0;
        config.target_month_score = 0;
        config.target_year_score  = 0;

        config.current_day_score   = 0;
        config.current_week_score  = 0;
        config.current_month_score = 0;
        config.current_year_score  = 0;

        write_settings(&config);
}
