#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>

#define CONFIG_PATH "./assets/config.ini"

#define TARGET_DAY_SCORE   "target_day_score"
#define TARGET_WEEK_SCORE  "target_week_score"
#define TARGET_MONTH_SCORE "target_month_score"
#define TARGET_YEAR_SCORE  "target_year_score"

#define CURRENT_DAY_SCORE   "current_day_score"
#define CURRENT_WEEK_SCORE  "current_week_score"
#define CURRENT_MONTH_SCORE "current_month_score"
#define CURRENT_YEAR_SCORE  "current_year_score"

typedef struct configurations {
        size_t target_day_score;
        size_t target_week_score;
        size_t target_month_score;
        size_t target_year_score;

        size_t current_day_score;
        size_t current_week_score;
        size_t current_month_score;
        size_t current_year_score;
} configurations;

void display_config(configurations config);
configurations *read_settings();
bool write_settings(const configurations *config);
void create_default_configs();

#endif
