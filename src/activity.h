#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <time.h>

#define MAX_TITLE_LENGTH 50
#define MAX_CATEGORY_LENGTH 20

#define NULL_TIME 0

typedef struct activity {
        char title[MAX_TITLE_LENGTH];
        size_t points;
        bool done;
        char category[MAX_CATEGORY_LENGTH];
        time_t time;
} activity;

void display_activity(const activity ac);
activity create_activity(char *title, size_t points, bool done, char *category);
activity *todo_activity(activity *ac);
activity *complete_activity(activity *ac);

#endif
