#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "activities.h"


#define BUFFER_SIZE 1024

const char *DELIM = "\t";

static char* activity_to_line(activity ac)
{
        char *line = (char*)calloc(BUFFER_SIZE, sizeof(char));

        if (ac.title == NULL) {
                perror("Activity cannot be displayed because title is NULL\n");
                return NULL;
        }

        if (ac.category == NULL) {
                perror("Activity cannot be displayed because category is NULL\n");
                return NULL;
        }

        sprintf(line, "%s%s%s%s%s%s%s%s%ld\n",  ac.title,
                                                DELIM,
                                                "2",
                                                DELIM,
                                                ac.done ? "1" : "0",
                                                DELIM,
                                                ac.category,
                                                DELIM,
                                                ac.time);

#ifdef DEBUG
        printf("line %s\n", line);
#endif

        return line;
}

static activity* line_to_activity(char* buffer)
{
        char *token;
        activity *ac = (activity*)malloc(sizeof(activity));

        if ((token = strtok(buffer, DELIM)) == NULL)
                return NULL;
        strncpy(ac->title, token, MAX_TITLE_LENGTH);

        if ((token = strtok(NULL, DELIM)) == NULL)
                return NULL;
        ac->points = atoi(token);

        if ((token = strtok(NULL, DELIM)) == NULL)
                return NULL;
        ac->done = atoi(token) == 1 ? true : false;

        if ((token = strtok(NULL, DELIM)) == NULL)
                return NULL;
        strncpy(ac->category, token, MAX_CATEGORY_LENGTH);

        if ((token = strtok(NULL, DELIM)) == NULL)
                return NULL;
        ac->time = atoi(token);

        return ac;
}

bool append_activity(activities_list **acl, const activity ac)
{
        activities_list *elem;
        activities_list *curr;

        if (acl == NULL &&
                        (acl = (activities_list**)malloc(sizeof(activities_list))) == NULL) {
                perror("No more free memory.");
                return false;
        }

        if ((elem = (activities_list*)malloc(sizeof(activities_list))) == NULL) {
                perror("Failed: no more free memory.");
                return false;
        }
        strncpy(elem->ac.title, ac.title, MAX_TITLE_LENGTH);
        elem->ac.points = ac.points;
        elem->ac.done = ac.done;
        strncpy(elem->ac.category, ac.category, MAX_CATEGORY_LENGTH);
        elem->ac.time = ac.time;
        elem->next = NULL;

        if (*acl == NULL) {
                *acl = elem;
        } else {
                /* Go to last element in list and append the new one. */
                curr = *acl;
                while (curr->next != NULL) curr++;
                curr->next = elem;
        }

        return true;
}

activities_list *get_activities()
{
        activities_list *activities = NULL;
        activity *ac;
        FILE *file = NULL;
        char buffer[BUFFER_SIZE];

        if ((file = fopen(ACTIVITIES_PATH, "r")) == NULL) {
                printf("%s\n", strerror(errno));
                return NULL;
        }

        while (fgets(buffer, BUFFER_SIZE, file) != NULL) {
                if ((ac = line_to_activity(buffer)) == NULL) {
                        perror("There was an error while creating activity from line in activities.txt.\n");
                        continue;
                }
                append_activity(&activities, *ac);
        }

        fclose(file);

        return activities;
}

activity *remove_activity(activities_list **acl, char *title)
{
        activities_list *prev = *acl;
        activities_list *curr;
        activity *ac;

        if (prev == NULL)
                return NULL;

        if (strncmp(prev->ac.title, title, MAX_TITLE_LENGTH) == 0) {
                ac = &(prev->ac);
                *acl = prev->next;
                return ac;
        }

        curr = prev->next;
        while (curr != NULL) {
                if (strncmp(curr->ac.title, title, MAX_TITLE_LENGTH) == 0) {
                        ac = &(curr->ac);
                        prev->next = curr->next;
                        break;
                }
                prev = curr;
                curr = curr->next;
        }

        return ac;
}

bool save_activities(activities_list *activities)
{
        FILE *file;
        activities_list *curr = activities;
        char *line;
        bool error = false;

        if ((file = fopen(ACTIVITIES_PATH, "w")) == NULL) {
                printf("%s\n", strerror(errno));
                return false;
        }

        while (curr != NULL) {
                if ((line = activity_to_line(curr->ac)) == NULL) {
                        error = true;
                        perror("line e' NULL\n");
                        break;
                }

                if (fputs(line, file) == EOF) {
                        error = true;
                        break;
                }

                curr = curr->next;
        }

        fclose(file);

        return !error;
}

void display_activities(activities_list *activities)
{
        activities_list *list = activities;

        if (activities == NULL) {
                printf("There is no activity.\n");
                return;
        }

        while (list != NULL) {
                display_activity(list->ac);
                printf("\n");
                list = list->next;
        }
}

/*
 * Clear activities file: file is opened in write mode so that it becomes empty.
 */
bool reset_activities()
{
        return fopen(ACTIVITIES_PATH, "w") != NULL;
}


activity *activity_done(activities_list *activities, char *title)
{
        activities_list *list = activities;

        if (activities == NULL) {
                printf("There is no activity.\n");
                return NULL;
        }

        while (list != NULL) {
                if (strncmp(title, list->ac.title, MAX_TITLE_LENGTH) == 0)
                        return complete_activity(&list->ac);

                list = list->next;
        }

        return NULL;
}

/*
 * Activities that are marked as done become marked as undone
 * if their category (daily, monthly, yearly) doesn't match current date.
 */
void activities_check()
{
        activities_list *activities, *list;
        struct tm *date, *curr_date;
        int year, month, day, weekday, curr_year, curr_month, curr_day, curr_weekday;
        time_t t;

        t = time(NULL);
        curr_date = localtime(&t);
        curr_year = 1900 + curr_date->tm_year;
        curr_month = curr_date->tm_mon;
        curr_day = curr_date->tm_mday;
        curr_weekday = curr_date->tm_wday;

        list = activities = get_activities();

        while (list != NULL) {
                if (list->ac.done) {
                        date = localtime(&list->ac.time);
                        year = 1900 + date->tm_year;
                        month = date->tm_mon;
                        day = date->tm_mday;
                        weekday = date->tm_wday;

                        if (strcmp(YEARLY, list->ac.category) == 0) {
                                if (year < curr_year)
                                        list->ac.done = false;
                        } else if (strcmp(MONTHLY, list->ac.category) == 0) {
                                if (year < curr_year || month < curr_month)
                                        list->ac.done = false;
                        } else if (strcmp(WEEKLY, list->ac.category) == 0) {
                                if (year < curr_year || month < curr_month || weekday < curr_weekday)
                                        list->ac.done = false;
                        } else if (strcmp(DAILY, list->ac.category) == 0) {
                                if (year < curr_year || month < curr_month || day < curr_day)
                                        list->ac.done = false;
                        }
                }

                list = list->next;
        }

        save_activities(activities);

        free(activities);
}
