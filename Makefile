BINARY := day_by_day

# Source code directory structure
BINDIR := bin
SRCDIR := src
LOGDIR := log
LIBDIR := lib
TESTDIR := test

# Test libraries
TEST_LIBS := -l cmocka -L /usr/lib

# Tests binary file
TEST_BINARY := $(BINARY)_test_runner

# Source code file extension
SRCEXT := c

# Defines the C Compiler
CC := gcc

STD := -std=c90

# Specifies to GCC the required warnings
WARNS := -Wall -Wextra -pedantic # -pedantic warns on language standards

DEBUG := -g

CFLAGS := $(STD) $(WARNS)

CFLAGS_TEST := -std=c99 $(WARNS)  # necessary for mocka.h


# %.o file names
NAMES := $(notdir $(basename $(wildcard $(SRCDIR)/*.$(SRCEXT))))
OBJECTS :=$(patsubst %,$(LIBDIR)/%.o,$(NAMES))



#
# COMPILATION RULES
#

default: all

# Help message
help:
	@echo "C Project Template"
	@echo
	@echo "Target rules:"
	@echo "    all      - Compiles and generates binary file"
	@echo "    tests    - Compiles with cmocka and run tests binary file"
	@echo "    clean    - Clean the project by removing binaries"
	@echo "    help     - Prints a help message with target rules"

# Rule for link and generate the binary file
all: $(OBJECTS)
	$(CC) -o $(BINDIR)/$(BINARY) $+ $(CFLAGS) $(LIBS)
	@echo "\nBinary file placed at $(BINDIR)/$(BINARY)\n";

debug: $(OBJECTS)
	$(CC) -o $(BINDIR)/$(BINARY) $+ $(DEBUG) $(CFLAGS) $(LIBS)
	@echo "\nBinary file placed at $(BINDIR)/$(BINARY)\n";

# Rule for object binaries compilation
$(LIBDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	$(CC) -c $^ -o $@ $(DEBUG) $(CFLAGS) $(LIBS)

# Compile tests and run the test binary
tests:
	$(CC) $(TESTDIR)/main.c -o $(BINDIR)/$(TEST_BINARY) $(DEBUG) $(CFLAGS_TEST) $(LIBS) $(TEST_LIBS)
	@which ldconfig && ldconfig -C /tmp/ld.so.cache || true # caching the library linking
	@echo -en "Running tests:";
	./$(BINDIR)/$(TEST_BINARY)

# Rule for cleaning the project
clean:
	@rm -rvf $(BINDIR)/* $(LIBDIR)/* $(LOGDIR)/*;
