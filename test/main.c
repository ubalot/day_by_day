#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>

#include "../src/activity.h"
#include "../src/activities.h"

/* include here your files that contain test functions */




/* A test case that does nothing and succeeds. */
static void null_test_success(void **state) {

    /**
     * If you want to know how to use cmocka, please refer to:
     * https://api.cmocka.org/group__cmocka__asserts.html
     */
    (void) state; /* unused */
}

// activity* create_activity(char *title, long unsigned int points, bool done, char *category) {
// 	// Always ensure input parameters are what're expected
// 	check_expected_ptr(title);
// 	check_expected(points);
// 	check_expected(done);
// 	check_expected_ptr(category);

// 	activity *ac = (activity*)malloc(sizeof(activity));

// 	if (ac == NULL)
// 		return NULL;

// 	ac->title = (char*)malloc(sizeof(char) * (strlen(title) + 1));
// 	strncpy(ac->title, title, strlen(title) + 1);

// 	ac->points = points;

// 	ac->done = done;

// 	ac->category = (char*)malloc(sizeof(char) * (strlen(category) + 1));
// 	strncpy(ac->category, category, strlen(category) + 1);

// 	return ac;
// }

activity* __wrap_create_activity() {
	char *title = "Title";
	long unsigned int points = 2;
	bool done = false;
	char *category = "daily";

	// activity *param = mock_ptr_type(activity *);
	// if (param == NULL) return false;

	// strcpy(param->title, title);
	// // return strcmp(param->title, title) == 0;
	// param->points = points;
	// // return (param->points == points);
	// param->done = done;
	// strcpy(param->category, category);

	// // assert_string_equal(param->title, title);
	// // assert_int_equal(param->points, points);
	// // assert_int_equal(param->done, done);
	// // assert_string_equal(param->category, category);

	activity ac = create_activity(title, points, done, category);

	/* assert_non_null(ac); */
	assert_string_equal(ac.title, title);
	assert_int_equal(ac.points, points);
	assert_int_equal(ac.done, done);
	assert_string_equal(ac.category, category);

	return &ac;
}

/*
activity* __wrap_copy_activity() {
	// char *title = "Title";
	// long unsigned int points = 2;
	// bool done = false;
	// char *category = "daily";

	// activity *ac = create_activity(title, points, done, category);
	activity *ac = __wrap_create_activity();

	activity *ac_copy;
	copy_activity(&ac_copy, ac);

	assert_non_null(ac_copy);
	assert_string_equal(ac_copy->title, ac->title);
	assert_int_equal(ac_copy->points, ac->points);
	assert_int_equal(ac_copy->done, ac->done);
	assert_string_equal(ac_copy->category, ac->category);

	return ac_copy;
}
*/
activities_list* __wrap_capture_activity() {
	activities_list *acl;

	char *title = "Title";
	long unsigned int points = 2;
	bool done = false;
	char *category = "daily";

	activity ac = create_activity(title, points, done, category);

	capture_activity(&acl, ac);

	assert_non_null(acl);

	return acl;
}

bool __wrap_append_activity() {
	activities_list *acl = __wrap_capture_activity();

	char *title = "Title2";
	long unsigned int points = 2;
	bool done = false;
	char *category = "daily";

	activity ac = create_activity(title, points, done, category);

	if (append_activity(&acl, ac) == false)
		return false;

	// Temporary pointer for visiting activities.
	activities_list *tmp = acl;

	// Check first activity.
	if (strcmp(tmp->ac.title, "Title") != 0)
		return false;

	// Check second activity.
	tmp = tmp->next;
	if (strcmp(tmp->ac.title, "Title2") != 0)
		return false;

	// Check NULL pointer at the end of the list.
	tmp = tmp->next;
	if (tmp != NULL)
		return false;

	return true;
}

bool __wrap_null_append_activity() {
	activities_list *acl = NULL;

	char *title = "Title";
	long unsigned int points = 2;
	bool done = false;
	char *category = "daily";

	activity ac = create_activity(title, points, done, category);

	if (append_activity(&acl, ac) == false)
		return false;

	if (acl == NULL)
		return false;

	// Temporary pointer for visiting activities.
	activities_list *tmp = acl;

	// Check first activity.
	if (strcmp(tmp->ac.title, "Title") != 0)
		return false;

	// Check NULL pointer at the end of the list.
	tmp = tmp->next;
	if (tmp != NULL)
		return false;

	return true;
}

static void create_activity_test(void ** state) {
	(void) state; /* unused */

	assert_non_null(__wrap_create_activity);
}

static void copy_activity_test(void **state) {
	(void) state; /* unused */

	/* assert_non_null(__wrap_copy_activity); */
}

static void create_activities_list_test(void **state) {
	(void) state; /* unused */

	assert_non_null(__wrap_capture_activity);
}

static void append_activity_test(void **state) {
	(void) state; /* unused */

	assert_true(__wrap_append_activity);
	assert_true(__wrap_null_append_activity);
}

/**
 * Test runner function
 */
int main(void) {

    /**
     * Insert here your test functions
     */
    const struct CMUnitTest tests[] = {
	cmocka_unit_test(null_test_success),
	cmocka_unit_test(create_activity_test),
	cmocka_unit_test(copy_activity_test),
	cmocka_unit_test(create_activities_list_test),
	cmocka_unit_test(append_activity_test),
    };


    /* Run the tests */
    return cmocka_run_group_tests(tests, NULL, NULL);
}